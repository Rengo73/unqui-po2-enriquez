package ar.edu.unq.po2.tp3;

import java.util.ArrayList;

public class Counter {

	//private int[] listaDeNumeros = new int[] {};
	private ArrayList<Integer> numeros = new ArrayList<Integer>();
	
	public Integer unNro;
	public int otroNro;
	public static int este;
	public static Integer otro;
	
	public void addNumber(int i) {
		this.numeros.add(i);
	}

	public Integer getEvenOccurrences() {
		Integer countEven = 0;
		for(Integer i: this.numeros){
			countEven += oneIfEven(i);
		}
		return countEven;
	}

	private Integer oneIfEven(Integer i) {
		if ( i%2 == 0) {
			return 1;
		}
		else return 0;
	}

}
