package ar.edu.unq.po2.tp3;

import java.util.ArrayList;

public class EquipoDeTrabajo {
	private ArrayList<Persona> integrantes = new ArrayList<Persona>();
	private String nombre;
	
	
	
	public EquipoDeTrabajo(String nombre) {
		super();
		this.nombre = nombre;
	}

	public Integer promedioDeEdades() {
		Integer suma=0;
		if (this.integrantes.isEmpty()) 
			return 0;
		for(Persona pers : this.integrantes) {
			suma += pers.getEdad();
		}
		return (suma/(this.integrantes.size()));
	}
	
	public String getNombre() {
		return this.nombre;
	}

	public void agregarIntegrante(Persona persona) {
		this.integrantes.add(persona);
	}
}
