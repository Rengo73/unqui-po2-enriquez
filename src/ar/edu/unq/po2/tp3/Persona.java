
package ar.edu.unq.po2.tp3;

import java.time.*;
import java.time.temporal.ChronoUnit;

public class Persona {
	private String nombre;
	private LocalDate fechaDeNacimiento;
	
	public Persona(String nombre, LocalDate fechaDeNacimiento) {
		super();
		this.nombre = nombre;
		this.fechaDeNacimiento = fechaDeNacimiento;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public LocalDate getFechaDeNacimiento() {
		return fechaDeNacimiento;
	}

	public void setFechaDeNacimiento(LocalDate fechaDeNacimiento) {
		this.fechaDeNacimiento = fechaDeNacimiento;
	}
	
	public Integer getEdad() {
		LocalDate now = LocalDate.now();
		Integer edad = (int) ChronoUnit.YEARS.between(now, this.getFechaDeNacimiento());
		return Math.abs(edad);
	}
	
	public Boolean esMayorQue(Persona otraPersona) {
		if (this.getEdad() > otraPersona.getEdad()) {
			return true;
		}
		else return false;
	}
	
}
