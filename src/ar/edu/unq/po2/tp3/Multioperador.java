package ar.edu.unq.po2.tp3;

import java.util.ArrayList;

public class Multioperador {
	public Integer suma(ArrayList<Integer> list){
		Integer sum = 0;
		for(Integer i : list) {
			sum += i;
		}
		return sum;
	}

	public Integer resta(ArrayList<Integer> list) {
		Integer res = 0;
		for(Integer i : list) {
			res -= i;
		}
		return res;
	}

	public Integer producto(ArrayList<Integer> list) {
		Integer prod = 1;
		for(Integer i : list) {
			prod *= i;
		}
		return prod;
	}
}
