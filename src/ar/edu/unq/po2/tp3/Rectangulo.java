package ar.edu.unq.po2.tp3;

public class Rectangulo {
	private Punto esquinaInferiorIzquierda = new Punto(); //esquina inferior izquierda
	private Integer base;
	private Integer altura;
	
	public Rectangulo(Punto origen, Integer base, Integer altura) {
		super();
		this.setOrigen(origen);
		this.setBase(base);
		this.setAltura(altura);
	}

	public Punto getOrigen() {
		return esquinaInferiorIzquierda;
	}

	public void setOrigen(Punto origen) {
		this.esquinaInferiorIzquierda = origen;
	}

	public Integer getBase() {
		return base;
	}

	public void setBase(Integer base) {
		this.base = base;
	}

	public Integer getAltura() {
		return altura;
	}

	public void setAltura(Integer altura) {
		this.altura = altura;
	}

	public Integer area() {
		return (this.getAltura() * this. getBase());
	}

	public Integer perimetro() {
		return (2*this.getAltura())+(2*this.getBase());
	}

	public boolean esHorizontal() {
		if (this.getBase()>=this.getAltura()) return true;
		return false;
	}
	
	public boolean esVertical() {
		if (this.getBase()<=this.getAltura()) return true;
		return false;
	}
	
	
}
