package ar.edu.unq.po2.tp4;

public class ProductoPrimeraNecesidad extends Producto {
	
	protected Double porcentajeDeDescuento;
	public ProductoPrimeraNecesidad(String nombre, Double precio, Boolean esPrecioCuidado, Double porcentajeDeDescuento) {
		super(nombre, precio, esPrecioCuidado);
		this.porcentajeDeDescuento=porcentajeDeDescuento;
	}

	public ProductoPrimeraNecesidad(String nombre, Double precio, Double porcentajeDeDescuento) {
		super(nombre, precio);
		this.porcentajeDeDescuento=porcentajeDeDescuento;
	}
	
	public Double getPrecio() {
		return this.precio-this.getDescuento();
	}

	private Double getDescuento() {
		return this.precio*this.porcentajeDeDescuento/100d;
	}
	
}
