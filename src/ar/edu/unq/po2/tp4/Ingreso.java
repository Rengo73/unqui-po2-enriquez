package ar.edu.unq.po2.tp4;

public class Ingreso {
	protected Integer mesDePercepcion;
	protected String concepto;
	protected Double montoPercibido;
	
	//Constructor
	public Ingreso(Integer mesDePercepcion, String concepto, Double montoPercibido) {
		super();
		this.setMesDePercepcion(mesDePercepcion);
		this.setConcepto (concepto);
		this.setMontoPercibido(montoPercibido);
	}
	
	//Getters y Setters de instancia
	public Integer getMesDePercepcion() {
		return mesDePercepcion;
	}

	public void setMesDePercepcion(Integer mesDePercepcion) {
		this.mesDePercepcion = mesDePercepcion;
	}

	public String getConcepto() {
		return concepto;
	}

	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}

	public Double getMontoPercibido() {
		return montoPercibido;
	}

	public void setMontoPercibido(Double montoPercibido) {
		this.montoPercibido = montoPercibido;
	}
	
	//Otros metodos de instancia
	public Double getMontoImponible() {
		return montoPercibido;
	}
	
}
