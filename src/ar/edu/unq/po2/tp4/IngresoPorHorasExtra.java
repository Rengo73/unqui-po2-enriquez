package ar.edu.unq.po2.tp4;

public class IngresoPorHorasExtra extends Ingreso{

	protected Integer cantidadDeHoras;
	
	public IngresoPorHorasExtra(Integer mesDePercepcion, String concepto, Double montoPercibido, Integer cantHoras) {
		super(mesDePercepcion, concepto, montoPercibido);
		this.setCantidadDeHoras(cantHoras);
	}
	
	public Integer getCantidadDeHoras() {
		return cantidadDeHoras;
	}
	
	public void setCantidadDeHoras(Integer cantidadDeHoras) {
		this.cantidadDeHoras = cantidadDeHoras;
	}
	
	public Double getMontoImponible() {
		return 0.0;
	}

}
