package ar.edu.unq.po2.tp4;

import java.util.ArrayList;

public class Trabajador {
	protected ArrayList<Ingreso> ingresos = new ArrayList<Ingreso>(); 
	
	public void addIngreso(Ingreso unIngreso) {
		ingresos.add(unIngreso);
	}
	
	public Double getTotalPercibido() {
		Double totalPercibido = 0.0;
		for (Ingreso i : ingresos) {
			totalPercibido += i.getMontoPercibido();
		}
		return totalPercibido;
	}
	
	public Double getMontoImponible() {
		Double totalImponible = 0.0;
		for (Ingreso i : ingresos) {
			totalImponible += i.getMontoImponible();
		}
		return totalImponible;
	}
	
	public Double getImpuestoAPagar() {
		return this.getMontoImponible() *0.02;
	}
}
