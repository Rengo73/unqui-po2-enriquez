package ar.edu.unq.po2.tp4;
import java.util.ArrayList;


public class Supermercado {
	private String nombre;
	private String direccion;
	private ArrayList<Producto> productos = new ArrayList<Producto>();
	
	public Supermercado(String nombre, String direccion) {
		super();
		this.nombre = nombre;
		this.direccion = direccion;
	}
	public Integer getCantidadDeProductos() {
		return this.productos.size();
	}
	public void agregarProducto(Producto unProducto) {
		this.productos.add(unProducto);
	}
	public Double getPrecioTotal() {
		Double suma=0d;
		for (Producto p : productos) {
			suma += p.getPrecio();
		}
		return suma;
	}
	
}
