package ar.edu.unq.po2.tp4;

public class Producto {
	protected String nombre;
	protected Boolean esPrecioCuidado = false;
	protected Double precio;
	
	public Producto(String nombre, Double precio) { //Constructor sin precio cuidado (false por defecto)
		super();
		this.nombre = nombre;
		this.precio = precio;
	}
	
	public Producto(String nombre, Double precio, Boolean esPrecioCuidado) { //Constructor para precio cuidado
		super();
		this.nombre = nombre;
		this.precio = precio;
		this.esPrecioCuidado = esPrecioCuidado;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Boolean esPrecioCuidado() {
		return esPrecioCuidado;
	}

//	public void setEsPrecioCuidado(Boolean esPrecioCuidado) {
//		this.esPrecioCuidado = esPrecioCuidado;
//	}

	public Double getPrecio() {
		return precio;
	}

	public void setPrecio(Double precio) {
		this.precio = precio;
	}

	public void aumentarPrecio(double cantidadQueAumenta) {
		this.setPrecio(this.getPrecio()+cantidadQueAumenta);
		
	}
	
	
	
	

}
