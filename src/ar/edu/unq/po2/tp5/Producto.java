package ar.edu.unq.po2.tp5;

public abstract class Producto implements Cobrable {
	private String nombre;
	private int stock;
	private double precio;
	
	//----Constructor
	public Producto(String nombre, int stock, double precio) {
		super();
		this.setNombre (nombre);
		this.setStock (stock);
		this.setPrecio (precio);
	}
	
	//-----Getters y setters
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public int getStock() {
		return stock;
	}
	public void setStock(int stock) {
		this.stock = stock;
	}
	public double getPrecio() {
		return precio;
	}
	public void setPrecio(double precio) {
		this.precio = precio;
	}
	
	//----Funcionamiento
	abstract public double comprarUno();
	
	protected void decrementarStock() {
		this.setStock(this.getStock()-1);
	}
}
