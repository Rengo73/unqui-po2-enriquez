package ar.edu.unq.po2.tp5;

public class FacturaDeServicios extends Factura {
	private double costoPorUnidad;
	private int unidadesConsumidas;
	
	public FacturaDeServicios(double costoPorUnidad, int unidadesConsumidas) {
		super();
		this.setCostoPorUnidad(costoPorUnidad);
		this.setUnidadesConsumidas(unidadesConsumidas);
	}	
	
	protected double getCostoPorUnidad() {
		return costoPorUnidad;
	}

	protected void setCostoPorUnidad(double costoPorUnidad) {
		this.costoPorUnidad = costoPorUnidad;
	}

	protected int getUnidadesConsumidas() {
		return unidadesConsumidas;
	}

	protected void setUnidadesConsumidas(int unidadesConsumidas) {
		this.unidadesConsumidas = unidadesConsumidas;
	}

	@Override
	public double comprarUno() {
		return this.getCostoPorUnidad()*this.getUnidadesConsumidas();
	}

}
