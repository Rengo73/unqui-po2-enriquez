package ar.edu.unq.po2.tp5;

import java.util.List;

public class Caja {
	private double montoTotalAPagar = 0;
	
	private Double registrar(Cobrable cobrable) {
		return cobrable.comprarUno();
	}
	
	public void registrarLista(List<Cobrable> lista) {
		for(Cobrable cobrable:lista) {
			this.setMontoTotalAPagar(this.registrar(cobrable) + this.getMontoTotalAPagar());
		}
	}

	public double getMontoTotalAPagar() {
		return (montoTotalAPagar);
	}

	private void setMontoTotalAPagar(double montoTotalAPagar) {
		this.montoTotalAPagar = montoTotalAPagar;
	}
	
	
	
}
