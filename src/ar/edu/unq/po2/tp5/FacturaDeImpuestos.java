package ar.edu.unq.po2.tp5;

public class FacturaDeImpuestos extends Factura {
	private double tasa;
	public FacturaDeImpuestos(double tasa){
		super();
		this.setTasa(tasa);
	}

	private void setTasa(double tasa) {
		this.tasa = tasa;
	}

	@Override
	public double comprarUno() {
		return this.getTasa();
	}

	private double getTasa() {
		return this.tasa;
	}

}
