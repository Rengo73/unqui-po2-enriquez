package ar.edu.unq.po2.tp5;

public class ProductoDeCooperativa extends Producto {

	public ProductoDeCooperativa(String nombre, int stock, double precio) {
		super(nombre, stock, precio);
	}
	
	@Override
	public double comprarUno () {
		this.decrementarStock();
		return (this.getPrecio() *(1.0-this.descuentoIVA()) );
	}
	
	private double descuentoIVA() {
		return 0.10; //10 por ciento
	}
	
	
}
