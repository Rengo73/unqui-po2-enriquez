package ar.edu.unq.po2.tp5;

public class ProductoTradicional extends Producto {

	public ProductoTradicional(String nombre, int stock, double precio) {
		super(nombre, stock, precio);
	}
	
	@Override
	public double comprarUno () {
		this.decrementarStock();
		return (this.getPrecio() );
	}

}
