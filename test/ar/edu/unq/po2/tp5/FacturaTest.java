package ar.edu.unq.po2.tp5;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class FacturaTest {

	FacturaDeImpuestos facImp;
	FacturaDeServicios facSrv;
	
	@BeforeEach
	void setUp() {
		facImp = new FacturaDeImpuestos(42.00);
		facSrv = new FacturaDeServicios(10.00, 80);
	}
	
	@Test
	void testConstructor() {
		assertFalse(null == facImp);
		assertFalse(null == facSrv);
	}
	
	@Test
	void testComprarUnoServicios() {
		assertEquals(facSrv.comprarUno(),800.00);
	}
	
	@Test
	void testComprarUnoImpuestos() {
		assertEquals(facImp.comprarUno(),42.00);
	}

}
