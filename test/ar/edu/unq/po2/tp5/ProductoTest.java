package ar.edu.unq.po2.tp5;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class ProductoTest {
	ProductoTradicional producto;
	ProductoDeCooperativa prodCoop;
	
	@BeforeEach
	void setUp() {
		producto = new ProductoTradicional("fideos",50,31.99);
		prodCoop = new ProductoDeCooperativa("aceite", 10, 80.00);
	}
	
	@Test
	void testConstructor() {
		assertEquals(producto.getNombre(),"fideos");
		assertEquals(producto.getStock(),50);
		assertEquals(producto.getPrecio(),31.99);
	}
	
	@Test
	void testComprarUnoTradi() {
		assertEquals(producto.comprarUno(),31.99);
		assertEquals(producto.getStock(),49);
	}
	
	@Test
	void testComprarUnoCoop() {
		assertEquals(prodCoop.comprarUno(),72.00 );
		assertEquals(prodCoop.getStock(), 9);
	}
	/*
	@Test
	void test3() {
		fail("Not yet implemented");
	}
	
	@Test
	void test4() {
		fail("Not yet implemented");
	}
	*/

}
