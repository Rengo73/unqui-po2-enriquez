package ar.edu.unq.po2.tp5;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class CajaTest {
	ArrayList<Cobrable> listaDeCompras;
	Caja caja;
	ProductoDeCooperativa prodCoop;
	ProductoTradicional prodTrad;
	FacturaDeImpuestos facImp;
	FacturaDeServicios facSrv;
	
	@BeforeEach
	void setUp() {
		listaDeCompras = new ArrayList<Cobrable>();
		caja = new Caja();
		prodTrad = new ProductoTradicional("fideos",50,32.00);
		prodCoop = new ProductoDeCooperativa("aceite", 10, 80.00);
		facImp = new FacturaDeImpuestos(42.00);
		facSrv = new FacturaDeServicios(10.00, 80);
	}
	@Test
	void testConstructor() {
		assertFalse(caja == null);
	}
	
	@Test
	void testNoHayProductos() {
		assertEquals(0.0,caja.getMontoTotalAPagar());
	}
	
	@Test
	void testHayUnProductoDeCooperativa() {
		listaDeCompras.add(prodCoop);
		caja.registrarLista(listaDeCompras);
		assertEquals(72.00, caja.getMontoTotalAPagar());
	}
	
	@Test
	void testHayUnProductoTradicional() {
		listaDeCompras.add(prodTrad);
		caja.registrarLista(listaDeCompras);
		assertEquals(32.00,caja.getMontoTotalAPagar());
	}
	
	@Test
	void testHayUnaFactDeImpYUnaDeServ() {
		listaDeCompras.add(facImp);
		listaDeCompras.add(facSrv);
		caja.registrarLista(listaDeCompras);
		assertEquals(842.00,caja.getMontoTotalAPagar());
	}
}