package ar.edu.unq.po2.tp3;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.Arrays;

import org.junit.jupiter.api.Test;

public class MultioperadorTestCase {
	private Multioperador mop = new Multioperador();
	
	
	@Test
	void testSuma() {
		ArrayList<Integer> list = new ArrayList<Integer>(Arrays.asList(1,2,3,4));
		assertEquals(mop.suma(list),10);
		//fail("Not yet implemented");
	}
	
	@Test
	void testResta() {
		ArrayList<Integer> list = new ArrayList<Integer>(Arrays.asList(1,2,3,4));
		assertEquals(mop.resta(list),-10);
	}
	
	@Test
	void testProducto() {
		ArrayList<Integer> list = new ArrayList<Integer>(Arrays.asList(1,2,3,4));
		assertEquals(mop.producto(list),24);
	}

}
