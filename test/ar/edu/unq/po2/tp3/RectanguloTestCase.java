package ar.edu.unq.po2.tp3;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class RectanguloTestCase {
	private Punto origen = new Punto();
	private Rectangulo unRectangulo;
	private Rectangulo otroRectangulo;
	private Cuadrado unCuadrado;
	
	@BeforeEach
	void setUp() {
		unRectangulo = new Rectangulo(origen ,2,3); //sera vertical
		otroRectangulo = new Rectangulo(origen ,3,2); // sera horizontal
		unCuadrado = new Cuadrado(origen,4);
	}
	@Test
	void testConstructor() {
		assertEquals(unRectangulo.getOrigen(),origen);
		assertEquals(unRectangulo.getBase(),2);
		assertEquals(unRectangulo.getAltura(),3);
		assertEquals(unCuadrado.getBase(),unCuadrado.getAltura());
	}
	
	@Test 
	void testArea() {
		assertEquals(unRectangulo.area(),6);
		assertEquals(unCuadrado.area(),16);
	}
	
	@Test
	void testPerimetro() {
		assertEquals(unRectangulo.perimetro(),10);
		assertEquals(unCuadrado.perimetro(),16);
	}
	
	@Test 
	void testVertical() {
		assertEquals(unRectangulo.esVertical(),true);
		assertEquals(otroRectangulo.esVertical(),false);
		assertEquals(unCuadrado.esVertical(),true);
	}
	
	@Test 
	void testHorizontal() {
		assertEquals(unRectangulo.esHorizontal(),false);
		assertEquals(otroRectangulo.esHorizontal(),true);
		assertEquals(unCuadrado.esHorizontal(),true);
	}

}
