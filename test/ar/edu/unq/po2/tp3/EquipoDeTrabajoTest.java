package ar.edu.unq.po2.tp3;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDate;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class EquipoDeTrabajoTest {
	private EquipoDeTrabajo edt;
	
	@BeforeEach
	void setUp() {
		edt = new EquipoDeTrabajo("Los Picapiedras");
		edt.agregarIntegrante( new Persona("Roberto", LocalDate.of(1991, 10, 15)));
		edt.agregarIntegrante( new Persona("Micaela", LocalDate.of(1981, 10, 15)));
		edt.agregarIntegrante( new Persona("Carla", LocalDate.of(1999, 10, 15)));
		edt.agregarIntegrante( new Persona("Nicolas", LocalDate.of(1989, 10, 15)));
		edt.agregarIntegrante( new Persona("Ramiro", LocalDate.of(1995, 10, 15)));
		
	}
	
	@Test
	void testPromedio() {
		assertEquals(28,edt.promedioDeEdades());
		System.out.println(edt.promedioDeEdades());
	}

}
