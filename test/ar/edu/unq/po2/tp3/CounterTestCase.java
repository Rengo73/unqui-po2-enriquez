package ar.edu.unq.po2.tp3;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class CounterTestCase {
	private Counter counter;
	/**
	 * Crea un escenario de test b�sico que consiste en un contador con 10 enteros
	 * @throws Exception
	 */
	@BeforeEach
	public void setUp() throws Exception{
		//crea el contador
		counter = new Counter();
		//Se agregan los numeros, 9 impares y 1 par
		counter.addNumber(1);
		counter.addNumber(3);
		counter.addNumber(5);
		counter.addNumber(7);
		counter.addNumber(9);
		counter.addNumber(1);
		counter.addNumber(1);
		counter.addNumber(1);
		counter.addNumber(1);
		counter.addNumber(4);
		
	}
	/**
	 * Verifica la cantidad de pares.
	 */
	@Test
	void testEvenNumbers() {
		Integer amount = counter.getEvenOccurrences();
		assertEquals(amount,1);
	}
	@Test
	void testIt() {
		String a = "abc";
		String s = a;
		String t;
		
		/** Ejer 2
		 * s.length() nos da un int que vale 3
		 * t.length() nos dira que t no est� inicializada
		 * 1+a nos da la string "1abc"
		 * a.toUpperCase nos da "ABC"
		 * "Libertad".indexOf("r"); busca una "r" y devuelve el numero de indice donde la encontro
		 * "Universidad".lastIndexOf('i');	busca 'i' y devuelve el indice, pero barre toda la string y tira el ultimo que encontro
		 * "Quilmes".substring(2,4); da la subcadena que se forma cortando la string en los indices dados
		 * (a.length()+a).startsWith("a"); denota un booleano, es f, porque la string empieza con el 3
		 * s == a;	denota un booleano, es v, porque las cadenas son iguales.
		 * a.substring(1,3).equals("bc"); denota un booleano, es v, tambien compara dos strings iguales
		 */
		
		
		/** Ejer 3
		 * Dato primitivo vs wrapper. Uno es una variable en una posicion de memoria en stack
		 * el otro pasa una referencia a un objeto instanciado que se guarda en heap.
		 * el Integer por defecto es Null y el int es 0, tanto para instancia como para clase.
		 */
		
		System.out.println( counter.unNro);
		System.out.println( counter.otroNro);
		
		//System.out.println( Counter.este);
		//System.out.println( Counter.otro);
		
	}
}
