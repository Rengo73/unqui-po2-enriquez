package ar.edu.unq.po2.tp3;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class PointTestCase {
	private Punto puntoVacio = new Punto();
	private Punto puntoLleno = new Punto(2,3);
	
	@Test
	void testConstructorVacio() {
		assertEquals(puntoVacio.getX(),0);
		assertEquals(puntoVacio.getY(),0);
	}
	@Test
	void testConstructorConParam() {
		assertEquals(puntoLleno.getX(),2);
		assertEquals(puntoLleno.getY(),3);
	}
	@Test
	void testMoverPunto() {
		puntoVacio.setXY(5, 6);
		assertEquals(puntoVacio.getX(),5);
		assertEquals(puntoVacio.getY(),6);
	}
	@Test 
	void testSumarPuntos() {
		Punto suma = new Punto();
		puntoVacio.setXY(5, 6);
		suma = puntoVacio.plus(puntoLleno);
		assertEquals(suma.getX(),7);
		assertEquals(suma.getY(),9);

	}

}
