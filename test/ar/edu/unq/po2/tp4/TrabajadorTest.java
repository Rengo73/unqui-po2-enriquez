package ar.edu.unq.po2.tp4;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class TrabajadorTest {


	Trabajador trabajador = new Trabajador();
	Ingreso ingresoNormal = new Ingreso(1,"Complementario 2",850.25);
	IngresoPorHorasExtra ingresoHorasExtra = new IngresoPorHorasExtra(2, "Venta de humo",400.00 , 4 );
	
	@BeforeEach
	void setUp() {
	}
	
	@Test
	void testTotalPercibido1() {
		//Caso sin ingresos
		assertEquals(0.0,trabajador.getTotalPercibido());
	}

	@Test
	void testTotalPercibido2() {
		//Unico ingreso normal de 850.25
		trabajador.addIngreso(ingresoNormal);
		assertEquals(850.25,trabajador.getTotalPercibido());
	}
	
	@Test
	void testTotalPercibido3() {
		//ingreso normal de 850.25 y hs extra de 400
		trabajador.addIngreso(ingresoNormal);
		trabajador.addIngreso(ingresoHorasExtra);
		assertEquals(1250.25,trabajador.getTotalPercibido());
	}
	
	@Test
	void testMontoImponible1() {
		//Sin ingresos
		assertEquals(0.0,trabajador.getMontoImponible());
	}
	@Test
	void testMontoImponible2() {
		//Con un unico ingreso normal de 850.25
		trabajador.addIngreso(ingresoNormal);
		assertEquals(850.25,trabajador.getMontoImponible());
	}
	@Test
	void testMontoImponible3() {
		//Con un unico Ingreso que no tributa 
		trabajador.addIngreso(ingresoHorasExtra);
		assertEquals(0.0,trabajador.getMontoImponible());
	}
	@Test
	void testMontoImponible4() {
		//Con un Ingreso que no tributa y otro que si de 850.25
		trabajador.addIngreso(ingresoHorasExtra);
		trabajador.addIngreso(ingresoNormal);
		assertEquals(850.25,trabajador.getMontoImponible());
	}
	
	@Test
	void testImpuestoAPagar1() {
		//Sin ingresos
		assertEquals(0.0,trabajador.getImpuestoAPagar());
	}
	@Test
	void testImpuestoAPagar2() {
		//Con un unico ingreso normal de 850.25
		trabajador.addIngreso(ingresoNormal);
		assertEquals(850.25*0.02,trabajador.getImpuestoAPagar());
	}
	@Test
	void testImpuestoAPagar3() {
		//Con un unico Ingreso que no tributa 
		trabajador.addIngreso(ingresoHorasExtra);
		assertEquals(0.0,trabajador.getImpuestoAPagar());
	}
	@Test
	void testImpuestoAPagar4() {
		//Con un Ingreso que no tributa y otro que si de 850.25
		trabajador.addIngreso(ingresoHorasExtra);
		trabajador.addIngreso(ingresoNormal);
		assertEquals(850.25*0.02,trabajador.getImpuestoAPagar());
	}
	
	
	
}
